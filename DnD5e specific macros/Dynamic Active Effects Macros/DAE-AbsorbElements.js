// Macro Author: Freeze (on discord)
// Macro version: 0.2
// Foundry version 9
// Requires: Item Macro, Times up, About time, MidiQOL and DAE.
// Works as a reaction damaged triggered event. Using MidiQOLs reactions.
// Set the action type of the spell to reaction damaged,
// Put this macro in the spell's Item Macro, and trigger it on Use, before Active Effects.
// On a triggered reaction the player gets a dialog where they choose the damage type.
// effect fades either on a hit or at the end of the next turn of the player.

new Dialog({
    title: "choose the element type:",
    content: "",
    buttons: {
        acid: {
            label: "Acid",
            callback: () => damageResistance("acid")
        },
        cold: {
            label: "Cold",
            callback: () => damageResistance("cold")
        },
        fire: {
            label: "Fire",
            callback: () => damageResistance("fire")
        },
        lightning: {
            label: "Lightning",
            callback: () => damageResistance("lightning")
        },
        thunder: {
            label: "Thunder",
            callback: () => damageResistance("thunder")
        },
    },
    
}).render(true);
async function damageResistance (elemType) {
    let data = {
        changes: [{
            key: `flags.midi-qol.DR.${elemType}`,
            mode: 2,
            priority: 20,
            value: "0"
        },
        {
            key: "flags.midi-qol.uncanny-dodge",
            mode: 2,
            priority: 20,
            value: "0"
        },
        {
            key: "data.bonuses.mwak.damage",
            mode: 2,
            priority: 20,
            value: `${args[0].spellLevel}d6[elemType]`
        }],
        duration: {
            rounds: 1
        },
        flags: {
            dae: {
                durationExpression: "",
                macroRepeat: "none",
                specialDuration: ["turnEndSource","1hit"],
                stackable: "none",
                transfer: false
            }
        },
        disabled: false,
        icon: "icons/magic/symbols/elements-air-earth-fire-water.webp",
        label: "Absorb Elements",
        tint: "",
        transfer: false
    };
    await token.actor.createEmbeddedDocuments("ActiveEffect", [data]);
}
