//buffs contains the data that is found in the changes of the active effect. This will be different from system to system.
//here i added 2 to strength, or 2 to defense (AC), or 10ft to walk speed. Based on the DND5e system.
const buffs = {
    str: [{
        key: "data.abilities.str.value",
        mode: 2,
        priority: 20,
        value: "2"
    }],
    def: [{
        key: "data.attributes.ac.bonus",
        mode: 2,
        priority: 20,
        value: "2"
    }],
    spd: [{
        key: "data.attributes.movement.walk",
        mode: 2,
        priority: 20,
        value: "10"
    }]
};

const effectLabel = "test buff"; // change this name

// there are plenty more keys that can be added, like duration for a temporary effect and such. And flags for things like DAE or Active Auras and such again depending on system.
const data = {
        label : effectLabel,
        icon : "icons/magic/symbols/symbol-lightning-bolt.webp",
        disabled: false
    };

const content = `<form>
    <div class="form-group">
        <label>Strength Buff</label>
        <div class="form-fields">
            <input type="radio" name="choice-radio" value="str" checked>
        </div>
    </div>
    <div class="form-group">
        <label>Defense Buff</label>
        <div class="form-fields">
            <input type="radio" name="choice-radio" value="def">
        </div>
    </div>
    <div class="form-group">
        <label>Speed Buff</label>
        <div class="form-fields">
            <input type="radio" name="choice-radio" value="spd">
        </div>
    </div>
</form>`;

new Dialog({
    title: "Buff selection",
    content,
    buttons: {
        ok: {
            label: "Ok",
            callback: async (html) => {
                const option = html.find("input[name=choice-radio]:checked").val();
                const effectData = {...data, changes: buffs[option]};
                await token.actor.createEmbeddedDocuments("ActiveEffect", [effectData]);
            }
        },
        cancel: {
            label: "Cancel"
        }
    }
}).render(true);
