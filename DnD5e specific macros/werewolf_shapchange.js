// Check all paths!!!!!!
// needs at LEAST the amazing modules: Warpgate and Sequencer,
// Benefits from these two modules: ItemMacro and/or MidiQOL to run from item instead of hotbar.
// Check all paths before concluding it doesnt work!

if(!token) return;
const beastName = "beast lycanshape"; // DO NOT CHANGE!
const humanName = "humanoid lycanshape"; // DO NOT CHANGE!
const animation = "modules/animated-spell-effects/spell-effects/air/smoke_explosion_CIRCLE_03.webm"; // get your own :)
const sound = "assets/Sounds/SFX/LycanChange.ogg"; // get your own too.
const humanImage = "assets/img/path/human.png"; // check where you store your favorite human image.
const wolfImage = "assets/img/path/wolf.png"; // check where you store your favorite wolf image.
// doesnt need werewolf image as werewolf is the default. So use this on something already a werewolf!

const tokenDoc = token.document;
const stack = warpgate.mutationStack(tokenDoc).stack;
const spearData = {
    type: "weapon",
    name: "Spear",
    img: "icons/weapons/polearms/spear-flared-worn-grey.webp",
    data: {
        ability: "str",
        actionType: "mwak",
        activation: {
            condition: "",
            cost: 1,
            type: "action"
        },
        attackBonus: 0,
        damage: {
            parts: [["1d8 + @mod", "piercing"]],
            versatile: "1d8 + @mod"
        },
        desription: {
            chat: "",
            unidentified: "",
            value: `<section class="secret">
                    <p>Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 ft., one creature. Hit: <strong>5 (1d6 + 2) <em>piercing damage</em></strong>, or <strong>6 (1d8 + 2) <em>piercing damage</em></strong> if used with two hands to make a melee attack.</p>
                    </section>
                    <p>The Commoner attacks with its Spear.</p>`
        },
        proficient: true,
        weaponType: "simpleM"
    }
};

new Dialog({
    title: "Morph",
    content: "",
    buttons: {
        "humanoid": {
            label: "Humanoid Shape",
            callback: async() => {
                animate(humanName);
            }
        },
        "hybrid":{
            label: "Hybrid Shape",
            callback: async() => {
                animate();
            }
        },
        "beast":{
            label: "Beast Shape",
            callback: async() => {
                animate(beastName);
            }
            
        }
    },
    render: listeners
}).render(true);


async function listeners(html) {
    if(stack.find(e => e.name === humanName)) {
        html.find(".humanoid").prop("disabled", true);
    }
    if(stack.find(e => e.name === beastName)) {
        html.find(".beast").prop("disabled", true);
    }
    if(stack.length === 0) {
        html.find(".hybrid").prop("disabled", true);
    }
}

async function change(type="hybrid"){
    if(type === "hybrid") {
        const previousForm = warpgate.mutationStack(tokenDoc).stack.find(e => e.name.includes("lycanshape")).name;
        return await warpgate.revert(tokenDoc, previousForm);
    }
    if(stack.find(e => e.name === beastName)) {
        await warpgate.revert(tokenDoc, beastName); 
    }
    if(stack.find(e => e.name === humanName)) {
        await warpgate.revert(tokenDoc, humanName); 
    }
    const updates = {
        token: {
            img: type === beastName ? wolfImage : humanImage,
            name: type === beastName ? "Wolf" : "Commoner",
        },
        actor: {
            name: type === beastName ? "Wolf" : "Commoner",
            img: type === beastName ? wolfImage : humanImage,
            data: type === beastName ? { attributes: { ac: { base: 12, flat: 12, value: 12} } } : { attributes: { ac: { base: 11, flat: 11, value: 11} } }
        },
        embedded: {
            Item: type === beastName ? { "Claws (Hybrid Form Only)":  warpgate.CONST.DELETE, "Multiattack (Humanoid or Hybrid Form Only)": warpgate.CONST.DELETE } 
                                     : {"Spear": spearData, "Claws (Hybrid Form Only)": warpgate.CONST.DELETE, "Bite (Wolf or Hybrid Form Only)": warpgate.CONST.DELETE }
        }
    };
    await warpgate.mutate(tokenDoc, updates, {}, {name: type});
}


async function animate(type) {
    new Sequence()
        .sound(sound)
        .effect()
            .file(animation)
            .atLocation(token)
            .playbackRate(1)
            .scale(0.75)
        .thenDo(async function(){
            await change(type);
        })
        .animation()
            .on(token)
            .playIf(() => {return token.data.width > 1;})
            .teleportTo({ 
                x: token.data.x + (0.5 + (token.data.width - 2)) * canvas.grid.size, 
                y: token.data.y + (0.5 + (token.data.height - 2)) * canvas.grid.size
            })
        
        .play();
}
