// macro requires Advanced Macros module.
// usage:1) make a script macro put this code inside.
//       2) name it something simple like myroll or mr
//       3) make a chat macro or type in the chat: /mr ?d6 + ?My Magical Modifier
//       4) Dialog appears and what was a ? in your roll formula will be the number you put in the dialog.
//       5) ?My Magical Modifier can be anything you want (preferably without numbers though, there are cases where those can go wrong).
//       6) Advanced rolls like ?d10x>8 or ?d20dl work too.

function getDice(diceString) {
    const diceFaces = diceString.substring(diceString.indexOf("?")+1);
    return diceFaces.trimEnd();
}
function getModifier(modString) {
    const modifier = modString.substring(modString.indexOf("?")+1);
    return modifier.trimEnd();
}

const regexp = /(?=\?).*?(?=\+|\-)|\?(?:.(?!\?))+$/gm;
let msg = args.join(" ");
const match = msg.match(regexp);
const content = match.reduce((acc, e, i) => {
    const label = e.search(/d\d+/) > -1 ? "Number of " + getDice(e) : getModifier(e);
    const name = `term-${i+1}`;
    acc += `<div class="form-group">
            <label>${label}:</label>
            <div class="form-fields">
                <input name="${name}" value="1">
            </div>
        </div>`;
    return acc;
}, "<form>") + "</form>";

new Dialog({
    title: "Dice Dialog",
    content,
    buttons: {
        roll: {
            label: "ROLL!",
            callback: (html) => {
                const values = match.map((e, i) => {
                    const name = `term-${i+1}`;
                    const value = html.find(`[name=${name}]`).val();
                    return value;
                });
                let i = 0;
                while(msg.includes("?")) {
                    msg = match[i].search(/d\d+/) > -1 ? msg.replace("?", values[i]) 
                                                     : msg.replace(match[i], values[i]);
                    i++;
                }
                const roll = msg.replaceAll(" ", "");
                new Roll(roll).toMessage();
            }
        }
    }
}).render(true);
